CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
The User Profile Override module allows site administrators to define which
user additional field present on user edit form by selecting checkboxes
settings in user field edit form.
For each system role can be set view and/or edit permission.
In field overview page will be presented a summery of role permissions.

REQUIREMENTS
------------
No requirements.

INSTALLATION
------------
Install as you would normally install a contributed drupal module.
See:
   https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

CONFIGURATION
-------------
The module has no modifiable settings or configuration.

MAINTAINERS
-----------
Current maintainers:
 * Kladrian - https://www.drupal.org/u/kladrian
